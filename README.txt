﻿Liste des Archevêchés et des Évêchés

Le programme TOPAMA fournit des données concernant la géographie historique de l’Europe de l'Ouest et de l’Afrique du Nord entre l’an 0 et 1500. 
Les données qu’il propose sont sérielles, datées, de grande précision (généralement à l’échelle de l’are) et au format ouvert sous la licence Odbl . 

L’équipe à l’œuvre se compose actuellement de Thomas Lienhard (LaMOP https://orcid.org/0000-0003-2253-7004), Armin Volkmann (Heidelberg), Willy Morice (LaMOP), Anne Laurent (IRHT), Mohamed Benabbès (Tunis), Pierre Brochard (LaMOP https://orcid.org/0000-0003-1955-556X) et Mathieu Beaud (LaMOP), mais une collaboration avec d’autres programmes (notamment le Digital Atlas of Roman and Medieval Civilization, Harvard, et le site vici.org) permettent d’obtenir des résultats plus rapides par échange de données. 

La liste des évêchés et archevêchés ne prétend pas être absolument complète. 
D’une part, le programme ne porte que sur les évêchés qui ont été catholiques à un moment ou un autre de leur histoire, excluant donc l’essentiel de l’espace orthodoxe. D’autre part, au stade actuel, n’ont été traités que les sièges qui se trouvent aujourd’hui dans l’un des pays suivants : Allemagne, Autriche, Belgique, Danemark, Espagne, France métropolitaine, Italie, Pays-Bas, Royaume-Uni, Slovaquie et Suisse. Enfin
Enfin, notre étude est dépendante de la documentation utilisée, en particulier :
    • C. Eubel, Hierarchia catholica medii aevi... ab anno 1198, Münster, 1898
    • P.B. Gams, Series episcoporum ecclesiae catholicae, Regensburg, 1873-86
    • F. Lanzoni, Le diocesi d'Italia dalle origini al principio del secolo VII (an. 604) : studio critico, Faenza, 1927 (disponible en ligne : http://archive.org/details/MN5017ucmf_0)
    • G. Cappelletti, Le Chiese d'Italia della loro origine sino ai nostri giorni, vol. IV, Venezia 1846, pp. 327-385
    • Wikipedia
    • https://www.katolsk.no

Les évêchés oubliés par ces grandes séries documentaires sont donc généralement absents de nos données. 

Les sièges répertoriés ont été appelés par leur nom actuel lorsqu’ils avaient survécu jusqu’à nos jours ; dans le cas contraire uniquement, on a recours à l’appellation antique ou médiévale.

Les dates fournies pour la création ou (le cas échéant) le transfert ou la suppression d’un siège n’ont qu’une précision relative. En particulier pour l’Antiquité ou les premiers siècles du Moyen Âge, la rareté des sources ne permet pas toujours une datation à l’année près. Deux options ont alors été adoptées :
- pour la France, on a choisi une datation au demi-siècle près (la date de 351 signifie ainsi « dans la seconde moitié du IVe siècle », sans prétendre à davantage de précision) ;
- pour les autres pays, on a retenu la première date pour laquelle le siège (ou son transfert ou sa suppression) était attesté ; lorsqu’il n’était pas certain que cette première mention corresponde effectivement à l’année de création (ou de transfert, etc.), on l’a fait précéder de la mention « TAQ » (terminus ante quem). Ces imprécisions concernent surtout l’Antiquité et l’époque mérovingienne ; mais encore pour les siècles suivants, les évêchés de l’Espagne musulmane continuent de présenter un grand degré d’incertitude.
Les modifications survenues après l’an 1500 n’ont pas été renseignées de manière systématique.

La localisation proposée pour chaque siège est celle de la cathédrale. Lorsque la documentation le permettait, on a géoréférencé la ou les cathédrale(s) médiévale(s) ; quand l’emplacement de celles-ci n’était pas connu, on a dû se contenter de la cathédrale moderne, même lorsqu’il n’était pas certain que celle-ci avait été construite sur le même site que son homologue médiévale ; dans les rares cas où il n’existe plus ni cathédrale moderne, ni renseignements à propos d’une cathédrale médiévale, on a géolocalisé uniquement le centre administratif de la commune concernée. Dans tous les cas, le système de coordonnées employé est celui de Google Maps, soit WGS84.

Pour l’espace italien, cette base de données doit beaucoup à Hugues Labarthe, qui est notamment le réalisateur de la rubrique « Pour un Atlas de la Chrétienté romaine » sur le site E-ecclesia.eu.

L'ensemble de ces fichiers sont distribuée en OpenData sous licence ODBL (https://opendatacommons.org/licenses/odbl/summary/) 

Contact : contact@lamop.fr

Notes: 

01/01/2019
Mise à jour de la base de données Topama - Liste des Archevêchés et des Évêchés

31/07/2012
Mise en ligne sur le site Menestrel
